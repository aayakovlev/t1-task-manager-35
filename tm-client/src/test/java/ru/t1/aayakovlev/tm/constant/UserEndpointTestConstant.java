package ru.t1.aayakovlev.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class UserEndpointTestConstant {

    @NotNull
    public final static String ADMIN_LOGIN = "admin";

    @NotNull
    public final static String ADMIN_PASSWORD = "admin";

    @NotNull
    public final static String TEST_LOGIN = "test1";

    @NotNull
    public final static String TEST_PASSWORD = "test1";

    @NotNull
    public final static String TEST_EMAIL = "test@test.ru";

    @NotNull
    public final static String TEST_LOGIN_TWO = "test2";

    @NotNull
    public final static String TEST_PASSWORD_TWO = "test2";

    @NotNull
    public final static String TEST_CHANGE_PASSWORD_TWO = "test22";

    @NotNull
    public final static String TEST_EMAIL_TWO = "test2@test.ru";

    @NotNull
    public final static String TEST_FIRST_NAME = "test first name";

    @NotNull
    public final static String TEST_LAST_NAME = "test last name";

    @NotNull
    public final static String TEST_MIDDLE_NAME = "test middle name";

}
