package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.request.ServerAboutRequest;
import ru.t1.aayakovlev.tm.dto.request.ServerVersionRequest;
import ru.t1.aayakovlev.tm.dto.response.ServerAboutResponse;
import ru.t1.aayakovlev.tm.dto.response.ServerVersionResponse;
import ru.t1.aayakovlev.tm.marker.IntegrationCategory;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

@Category(IntegrationCategory.class)
public final class SystemEndpointImplTest {

    @NotNull
    private final PropertyService service = new PropertyServiceImpl();

    @NotNull
    private final SystemEndpoint systemEndpoint = SystemEndpoint.newInstance(service);

    @Test
    public void When_GetAbout_Expect_ServerAboutResponse() {
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        @NotNull final ServerAboutResponse response = systemEndpoint.getAbout(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getName());
        Assert.assertNotNull(response.getEmail());
        Assert.assertNotNull(response.getGitBranch());
        Assert.assertNotNull(response.getGitCommitId());
        Assert.assertNotNull(response.getGitCommitMessage());
        Assert.assertNotNull(response.getGitCommitterEmail());
        Assert.assertNotNull(response.getGitCommitterName());
        Assert.assertNotNull(response.getGitCommitTime());
        Assert.assertNotNull(response.getGitCommitMessage());
    }

    @Test
    public void When_GetVersion_Expect_ServerVersionResponse() {
        @NotNull final ServerVersionRequest request = new ServerVersionRequest();
        @Nullable final ServerVersionResponse response = systemEndpoint.getVersion(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getVersion());
    }

}
