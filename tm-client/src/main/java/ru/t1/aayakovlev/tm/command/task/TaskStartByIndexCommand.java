package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.TaskStartByIndexRequest;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Start task by index.";

    @NotNull
    public static final String NAME = "task-start-by-index";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY INDEX]");
        System.out.print("Enter index: ");
        @NotNull final Integer index = nextNumber() - 1;

        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatus(Status.IN_PROGRESS);

        getTaskEndpoint().startByIndex(request);
    }

}
