package ru.t1.aayakovlev.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Passed argument not recognized...");
    }

    public ArgumentNotSupportedException(@NotNull final String argument) {
        super("Error! Passed argument '" + argument + "' not supported...");
    }

}
