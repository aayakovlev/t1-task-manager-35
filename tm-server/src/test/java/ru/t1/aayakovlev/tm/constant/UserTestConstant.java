package ru.t1.aayakovlev.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public final class UserTestConstant {

    @NotNull
    public final static User ADMIN_USER_ONE = new User();

    @NotNull
    public final static User ADMIN_USER_TWO = new User();

    @NotNull
    public final static User COMMON_USER_ONE = new User();

    @NotNull
    public final static User COMMON_USER_TWO = new User();

    @NotNull
    public final static User USER_NOT_EXISTED = new User();

    @NotNull
    public final static String PASSWORD = "Expected password";

    @NotNull
    public final static String PASSWORD_HASH = "bc345183cf12879505b712771cfae9e8";

    @NotNull
    public final static String FIRST_NAME = "Expected first name";

    @NotNull
    public final static String LAST_NAME = "Expected last name";

    @NotNull
    public final static String MIDDLE_NAME = "Expected middle name";

    @NotNull
    public final static String USER_ID_NOT_EXISTED = USER_NOT_EXISTED.getId();

    @NotNull
    public final static List<User> ADMIN_USER_LIST = Arrays.asList(ADMIN_USER_ONE, ADMIN_USER_TWO);

    @NotNull
    public final static List<User> COMMON_USER_LIST = Arrays.asList(COMMON_USER_ONE, COMMON_USER_TWO);

    @NotNull
    public final static List<User> COMMON_USER_SORTED_LIST = new ArrayList<>(COMMON_USER_LIST);

    @NotNull
    public final static List<User> USER_LIST = new ArrayList<>();

    @NotNull
    public final static List<User> USER_SORTED_LIST = new ArrayList<>();

    static {
        ADMIN_USER_LIST.forEach(
                user -> {
                    user.setRole(Role.ADMIN);
                    user.setLogin("Admin " + user.hashCode());
                    user.setEmail("Admin email " + user.hashCode());
                }
        );
        COMMON_USER_LIST.forEach(
                user -> {
                    user.setRole(Role.USUAL);
                    user.setLogin("Common " + user.hashCode());
                    user.setEmail("Common email " + user.hashCode());
                }
        );

        USER_LIST.addAll(ADMIN_USER_LIST);
        USER_LIST.addAll(COMMON_USER_LIST);

        COMMON_USER_SORTED_LIST.sort(Comparator.comparing(AbstractModel::getId));

        USER_SORTED_LIST.addAll(USER_LIST);
        USER_SORTED_LIST.sort(Comparator.comparing(AbstractModel::getId));
    }

}
