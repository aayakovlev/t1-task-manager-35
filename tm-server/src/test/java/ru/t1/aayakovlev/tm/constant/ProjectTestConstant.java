package ru.t1.aayakovlev.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.comparator.NameComparator;
import ru.t1.aayakovlev.tm.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

public final class ProjectTestConstant {

    @NotNull
    public final static Project PROJECT_USER_ONE = new Project();

    @NotNull
    public final static Project PROJECT_USER_TWO = new Project();


    @NotNull
    public final static Project PROJECT_ADMIN_ONE = new Project();

    @NotNull
    public final static Project PROJECT_ADMIN_TWO = new Project();

    @NotNull
    public final static Project PROJECT_NOT_EXISTED = new Project();

    @NotNull
    public final static String PROJECT_ID_NOT_EXISTED = PROJECT_NOT_EXISTED.getId();

    @NotNull
    public final static String NAME = "Expected Project name";

    @NotNull
    public final static String DESCRIPTION = "Expected Project Description";

    @NotNull
    public final static List<Project> ADMIN_PROJECT_LIST = Arrays.asList(PROJECT_ADMIN_ONE, PROJECT_ADMIN_TWO);

    @NotNull
    public final static List<Project> USER_PROJECT_LIST = Arrays.asList(PROJECT_USER_ONE, PROJECT_USER_TWO);

    @NotNull
    public final static List<Project> USER_PROJECT_SORTED_LIST = new ArrayList<>(USER_PROJECT_LIST);

    @NotNull
    public final static List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    public final static List<Project> PROJECT_SORTED_LIST = new ArrayList<>();

    static {
        ADMIN_PROJECT_LIST.forEach(
                project -> {
                    project.setName("Test Admin Project " + project.hashCode());
                    project.setDescription("Test Admin Project " + project.hashCode());
                    project.setUserId(ADMIN_USER_ONE.getId());
                }
        );
        USER_PROJECT_LIST.forEach(
                project -> {
                    project.setName("Test User Project " + project.hashCode());
                    project.setDescription("Test User Project " + project.hashCode());
                    project.setUserId(COMMON_USER_ONE.getId());
                }
        );

        PROJECT_LIST.addAll(ADMIN_PROJECT_LIST);
        PROJECT_LIST.addAll(USER_PROJECT_LIST);

        USER_PROJECT_SORTED_LIST.sort(NameComparator.INSTANCE);

        PROJECT_SORTED_LIST.addAll(PROJECT_LIST);
        PROJECT_SORTED_LIST.sort(NameComparator.INSTANCE);
    }

}
