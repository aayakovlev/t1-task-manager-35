package ru.t1.aayakovlev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.PROJECT_ID_NOT_EXISTED;
import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.PROJECT_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

@Category(UnitCategory.class)
public final class TaskRepositoryImplTest {

    @NotNull
    private final TaskRepository repository = new TaskRepositoryImpl();

    @Before
    public void init() throws EntityEmptyException {
        repository.save(TASK_USER_ONE);
        repository.save(TASK_USER_TWO);
    }

    @After
    public void after() {
        repository.removeAll(TASK_LIST);
    }

    @Test
    public void When_FindByIdExistsTask_Expect_ReturnTask() throws AbstractException {
        @Nullable final Task task = repository.findById(COMMON_USER_ONE.getId(), TASK_USER_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_USER_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_USER_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_USER_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_USER_ONE.getUserId(), task.getUserId());
        Assert.assertEquals(TASK_USER_ONE.getCreated(), task.getCreated());
    }

    @Test
    public void When_FindByIdExistsTask_Expect_ReturnNull() throws AbstractException {
        @Nullable final Task task = repository.findById(TASK_ID_NOT_EXISTED);
        Assert.assertNull(task);
    }

    @Test
    public void When_SaveNotNullTask_Expect_ReturnTask() throws AbstractException {
        @NotNull final Task savedTask = repository.save(ADMIN_USER_ONE.getId(), TASK_ADMIN_ONE);
        Assert.assertNotNull(savedTask);
        Assert.assertEquals(TASK_ADMIN_ONE, savedTask);
        @Nullable final Task task = repository.findById(ADMIN_USER_ONE.getId(), TASK_ADMIN_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_ADMIN_ONE, task);
    }

    @Test
    public void When_CountCommonUserTasks_Expect_ReturnTwo() throws AbstractException {
        final int count = repository.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedTask_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(TASK_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedTask_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(TASK_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedTask_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId(), TASK_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedTask_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId(), TASK_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListTasks() throws UserIdEmptyException {
        final List<Task> Tasks = repository.findAll(COMMON_USER_ONE.getId());
        Assert.assertArrayEquals(Tasks.toArray(), USER_TASK_LIST.toArray());
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedTaskList() throws UserIdEmptyException {
        @NotNull final List<Task> tasks = repository.findAll(COMMON_USER_ONE.getId(), Sort.BY_NAME.getComparator());
        Assert.assertArrayEquals(tasks.toArray(), USER_TASK_SORTED_LIST.toArray());
    }

    @Test
    public void When_FindByIndexExistedIndex_Expect_ReturnTask() throws AbstractException {
        @Nullable final Task task = repository.findByIndex(COMMON_USER_ONE.getId(), 1);
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_USER_TWO.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_USER_TWO.getName(), task.getName());
        Assert.assertEquals(TASK_USER_TWO.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_USER_TWO.getUserId(), task.getUserId());
        Assert.assertEquals(TASK_USER_TWO.getCreated(), task.getCreated());
    }

    @Test
    public void When_RemoveExistedTask_Expect_ReturnTask() throws AbstractException {
        Assert.assertNotNull(repository.save(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO));
        Assert.assertNotNull(repository.remove(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO));
    }

    @Test
    public void When_RemoveNotTask_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.remove(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED)
        );
    }

    @Test
    public void When_RemoveAll_Expect_ZeroCountTasks() throws AbstractException {
        repository.save(TASK_ADMIN_ONE);
        repository.save(TASK_ADMIN_TWO);
        repository.removeAll(ADMIN_USER_ONE.getId());
        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    public void When_RemoveByIdExistedTask_Expect_Task() throws AbstractException {
        Assert.assertNotNull(repository.save(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO));
        Assert.assertNotNull(repository.removeById(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO.getId()));
    }

    @Test
    public void When_RemoveByIdNotExistedTask_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.removeById(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId())
        );
    }

    @Test
    public void When_RemoveByIndexExistedIndex_Expect_ReturnTask() throws AbstractException {
        repository.save(TASK_ADMIN_TWO);
        final int count = repository.count(ADMIN_USER_ONE.getId());
        Assert.assertNotNull(repository.findByIndex(ADMIN_USER_ONE.getId(), count - 1));
    }

    @Test
    public void When_CreateNameTask_Expect_ExistedTask() {
        @Nullable final Task task = repository.create(ADMIN_USER_ONE.getId(), NAME);
        Assert.assertNotNull(task);
        Assert.assertEquals(NAME, task.getName());
    }

    @Test
    public void When_CreateNameDescriptionTask_Expect_ExistedTask() {
        @Nullable final Task task = repository.create(ADMIN_USER_ONE.getId(), NAME, DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(NAME, task.getName());
        Assert.assertEquals(DESCRIPTION, task.getDescription());
    }

    @Test
    public void When_FindAllByProjectIdExistedProject_Expect_ReturnTaskList() {
        @NotNull final List<Task> tasks = repository.findAllByProjectId(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
        Assert.assertNotEquals(0, tasks.size());
    }

    @Test
    public void When_FindAllByProjectIdNotExistedProject_Expect_ReturnEmptyList() {
        @NotNull final List<Task> tasks = repository.findAllByProjectId(COMMON_USER_ONE.getId(), PROJECT_ID_NOT_EXISTED);
        Assert.assertEquals(0, tasks.size());
    }

}
