package ru.t1.aayakovlev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.impl.SessionRepositoryImpl;

import java.util.Comparator;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.SessionTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

@Category(UnitCategory.class)
public final class SessionRepositoryImplTest {

    @NotNull
    private final SessionRepositoryImpl repository = new SessionRepositoryImpl();

    @Before
    public void init() {
        repository.save(SESSION_USER_ONE);
        repository.save(SESSION_USER_TWO);
    }

    @After
    public void after() {
        repository.removeAll(SESSION_LIST);
    }

    @Test
    public void When_FindByIdExistsSESSION_Expect_ReturnProject() throws AbstractException {
        final @Nullable Session project = repository.findById(COMMON_USER_ONE.getId(), SESSION_USER_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(SESSION_USER_ONE.getUserId(), project.getUserId());
    }

    @Test
    public void When_FindByIdExistsSESSION_Expect_ReturnNull() throws AbstractException {
        @Nullable final Session project = repository.findById(SESSION_ID_NOT_EXISTED);
        Assert.assertNull(project);
    }

    @Test
    public void When_SaveNotNullSESSION_Expect_ReturnProject() throws AbstractException {
        @NotNull final Session savedProject = repository.save(ADMIN_USER_ONE.getId(), SESSION_ADMIN_ONE);
        Assert.assertNotNull(savedProject);
        Assert.assertEquals(SESSION_ADMIN_ONE, savedProject);
        @Nullable final Session project = repository.findById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(SESSION_ADMIN_ONE, project);
    }

    @Test
    public void When_CountCommonUserProjects_Expect_ReturnTwo() {
        final int count = repository.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedSESSION_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedSESSION_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedSESSION_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId(), SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedSESSION_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId(), SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListProjects() {
        final List<Session> projects = repository.findAll(COMMON_USER_ONE.getId());
        Assert.assertArrayEquals(projects.toArray(), USER_SESSION_LIST.toArray());
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedProjectList() {
        @NotNull final List<Session> projects = repository.findAll(
                COMMON_USER_ONE.getId(),
                Comparator.comparing(Session::getId)
        );
        Assert.assertArrayEquals(projects.toArray(), USER_SESSION_SORTED_LIST.toArray());
    }

    @Test
    public void When_FindByIndexExistedIndex_Expect_ReturnProject() {
        @Nullable final Session project = repository.findByIndex(COMMON_USER_ONE.getId(), 1);
        Assert.assertNotNull(project);
        Assert.assertEquals(SESSION_USER_TWO.getUserId(), project.getUserId());
    }

    @Test
    public void When_RemoveExistedSESSION_Expect_ReturnProject() throws AbstractException {
        Assert.assertNotNull(repository.save(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO));
        Assert.assertNotNull(repository.remove(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO));
    }

    @Test
    public void When_RemoveNotSESSION_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.remove(ADMIN_USER_ONE.getId(), SESSION_NOT_EXISTED)
        );
    }

    @Test
    public void When_RemoveAll_Expect_ZeroCountProjects() {
        repository.save(SESSION_ADMIN_ONE);
        repository.save(SESSION_ADMIN_TWO);
        repository.removeAll(ADMIN_USER_ONE.getId());
        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    public void When_RemoveByIdExistedSESSION_Expect_Project() throws AbstractException {
        Assert.assertNotNull(repository.save(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO));
        Assert.assertNotNull(repository.removeById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO.getId()));
    }

    @Test
    public void When_RemoveByIdNotExistedSESSION_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.removeById(ADMIN_USER_ONE.getId(), SESSION_NOT_EXISTED.getId())
        );
    }

}
