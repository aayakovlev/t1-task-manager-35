package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;
import ru.t1.aayakovlev.tm.repository.UserOwnedRepository;

import java.util.List;

public interface UserOwnedService<M extends AbstractUserOwnedModel> extends BaseService<M>, UserOwnedRepository<M> {

    void clear(@Nullable final String userId) throws UserIdEmptyException;

    @NotNull
    List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) throws UserIdEmptyException;

    @NotNull
    M findById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    M findByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractException;

    @NotNull
    M removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    M removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractException;

}
