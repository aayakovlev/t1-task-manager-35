package ru.t1.aayakovlev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface TaskEndpoint extends BaseEndpoint {

    @NotNull
    String NAME = "TaskEndpointImpl";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static TaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static TaskEndpoint newInstance(@NotNull final ConnectionProvider connectionProvider) {
        return BaseEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, TaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static TaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return BaseEndpoint.newInstance(host, port, NAME, SPACE, PART, TaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskChangeStatusByIndexResponse changeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskClearResponse clear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskCompleteByIdResponse completeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskCompleteByIndexResponse completeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskCreateResponse create(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskRemoveByIndexResponse removeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskShowAllResponse showAll(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowAllRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskShowByIdResponse showById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskShowByIndexResponse showByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskShowByProjectIdResponse showByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByProjectIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskStartByIdResponse startById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskStartByIndexResponse startByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIndexRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    TaskUpdateByIndexResponse updateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIndexRequest request
    ) throws AbstractException;

}
