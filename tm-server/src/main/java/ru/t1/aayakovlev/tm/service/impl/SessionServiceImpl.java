package ru.t1.aayakovlev.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.SessionRepository;
import ru.t1.aayakovlev.tm.service.SessionService;

public final class SessionServiceImpl extends AbstractUserOwnedService<Session, SessionRepository> implements SessionService {

    public SessionServiceImpl(@NotNull final SessionRepository repository) {
        super(repository);
    }

}
