package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.EmailEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.model.User;

public interface UserService extends BaseService<User> {

    @NotNull
    User create(@Nullable final String login, @Nullable final String password) throws AbstractException;

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException;

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException;

    @Nullable
    User findByLogin(@Nullable final String login) throws AbstractException;

    @Nullable
    User findByEmail(@Nullable final String email) throws AbstractException;

    boolean isLoginExists(@Nullable final String login) throws LoginEmptyException;

    boolean isEmailExists(@Nullable final String email) throws EmailEmptyException;

    @NotNull
    User lockUserByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    User removeByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    User removeByEmail(@Nullable final String email) throws AbstractException;

    @NotNull
    User setPassword(@Nullable final String id, @Nullable final String password) throws AbstractException;

    @NotNull
    User unlockUserByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    User updateUser(
            @Nullable final String id,
            @Nullable final String firstname,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException;

}
