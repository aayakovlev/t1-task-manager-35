package ru.t1.aayakovlev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.UserLoginRequest;
import ru.t1.aayakovlev.tm.dto.request.UserLogoutRequest;
import ru.t1.aayakovlev.tm.dto.request.UserShowProfileRequest;
import ru.t1.aayakovlev.tm.dto.response.UserLoginResponse;
import ru.t1.aayakovlev.tm.dto.response.UserLogoutResponse;
import ru.t1.aayakovlev.tm.dto.response.UserShowProfileResponse;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface AuthEndpoint extends BaseEndpoint {

    @NotNull
    String NAME = "AuthEndpointImpl";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static AuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static AuthEndpoint newInstance(@NotNull final ConnectionProvider connectionProvider) {
        return BaseEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, AuthEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static AuthEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return BaseEndpoint.newInstance(host, port, NAME, SPACE, PART, AuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserShowProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserShowProfileRequest request
    ) throws AbstractException;

}
