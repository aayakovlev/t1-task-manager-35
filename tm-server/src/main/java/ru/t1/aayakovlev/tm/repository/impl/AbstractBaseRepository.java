package ru.t1.aayakovlev.tm.repository.impl;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.repository.BaseRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractBaseRepository<M extends AbstractModel> implements BaseRepository<M> {

    @NotNull
    protected List<M> models = new ArrayList<>();

    @Override
    @NotNull
    public Collection<M> add(@NotNull final Collection<M> models) {
        this.models.addAll(models);
        return this.models;
    }

    @Override
    public void clear() {
        this.models.clear();
    }

    @Override
    public boolean existsById(@NotNull final String id) throws AbstractFieldException {
        return findById(id) != null;
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return models;
    }

    @Override
    @Nullable
    public M findById(@NotNull final String id) throws AbstractFieldException {
        return models.stream()
                .filter((m) -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @NotNull
    public M remove(@NotNull final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public void removeAll(@NotNull final List<M> models) {
        this.models.removeAll(models);
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String id) throws AbstractException {
        final M model = findById(id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Override
    @NotNull
    public M save(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

}
